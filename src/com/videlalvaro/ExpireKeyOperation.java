package com.videlalvaro;

import java.util.concurrent.atomic.AtomicBoolean;

public class ExpireKeyOperation<K extends Comparable<K>> extends TimerTask {

    private AtomicBoolean completed = new AtomicBoolean(false);

    private K key;
    private ExpireMap<K, Object> expireMap;


    public ExpireKeyOperation(K key, ExpireMap expireMap, long timeoutMs) {
        this.key = key;
        this.expireMap = expireMap;
        this.setExpirationMs(timeoutMs + System.currentTimeMillis());
    }

    public boolean forceComplete() {
        if (completed.compareAndSet(false, true)) {
            // removes Task from its TimingWheel bucket.
            cancel();
            onComplete();
            return true;
        } else {
            return false;
        }
    }

    public boolean isCompleted() {
        return completed.get();
    }

    public void onComplete() {
        expireMap.remove(key);
    }

    @Override
    public void run() {
        forceComplete();
    }
}
