package com.videlalvaro;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ExpireMap<K extends Comparable<K>, V> {
    private Node root;

    private class Node {
        private K key;
        private V val;
        private Node left, right;
        private int N;
        private ExpireKeyOperation expireKeyOperation;

        public Node(K key, V val, int N, ExpireKeyOperation expireKeyOperation) {
            this.key = key;
            this.val = val;
            this.N = N;
            this.expireKeyOperation = expireKeyOperation;
        }
    }

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock r = lock.readLock();
    private final Lock w = lock.writeLock();

    private ExecutorService executor = Executors.newFixedThreadPool(1, new ThreadFactory() {
        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "executor");
            thread.setDaemon(false);
            thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread t, Throwable e) {
                    System.out.println("Uncaught exception in thread '" + t.getName() + "':" + e.getMessage());
                }
            });
            return thread;
        }
    });

    private Timer timeoutTimer = new Timer(executor, 1L, 20, System.currentTimeMillis());

    private ExpiredKeyReaper expirationReaper = new ExpiredKeyReaper(this.toString(), false);

    public ExpireMap() {
        expirationReaper.start();
    }

    public void put(K key, V val, long timeoutMs) {
        root = put(root, key, val, timeoutMs);
    }

    public Node put(Node x, K key, V value, long timeoutMs) {
        w.lock();
        try {
            if (x == null) {
                ExpireKeyOperation expireKeyOperation = new ExpireKeyOperation<>(key, this, timeoutMs);
                timeoutTimer.add(expireKeyOperation);
                return new Node(key, value, 1, expireKeyOperation);
            }

            int cmp = key.compareTo(x.key);

            if (cmp < 0) {
                x.left = put(x.left, key, value, timeoutMs);
            } else if (cmp > 0) {
                x.right = put(x.right, key, value, timeoutMs);
            } else {
                x.val = value;
                x.expireKeyOperation.cancel();
                ExpireKeyOperation expireKeyOperation = new ExpireKeyOperation<>(key, this, timeoutMs);
                timeoutTimer.add(expireKeyOperation);
                x.expireKeyOperation = expireKeyOperation;
            }
            x.N = 1 + size(x.left) + size(x.right);
            return x;
        } finally {
            w.unlock();
        }
    }

    public V get(K key) {
        return get(root, key);
    }

    public V get(Node x, K key) {
        r.lock();
        try {
            if (x == null) return null;
            int cmp = key.compareTo(x.key);
            if      (cmp < 0) {
                return get(x.left, key);
            }
            else if (cmp > 0) {
                return get(x.right, key);
            }
            else {
                return x.val;
            }
        } finally {
            r.unlock();
        }
    }

    public void remove(K key) {
        root = remove(root, key);
    }

    public Node remove(Node x, K key) {
        w.lock();
        try {
            if (x == null) return null;
            int cmp = key.compareTo(x.key);
            if (cmp < 0) {
                x.left = remove(x.left, key);
            } else if (cmp > 0) {
                x.right = remove(x.right, key);
            } else {
                x.expireKeyOperation.cancel();
                if (x.right == null) {
                    return x.left;
                }
                if (x.left == null) {
                    return x.right;
                }
                Node t = x;
                x = min(t.right);
                x.right = deleteMin(t.right);
                x.left = t.left;
            }
            x.N = size(x.left) + size(x.right) + 1;
            return x;
        } finally {
            w.unlock();
        }
    }

    private int size(Node x) {
        if (x == null) {
            return 0;
        } else {
            return x.N;
        }
    }

    private Node min(Node x) {
        if (x.left == null) {
            return x;
        } else {
            return min(x.left);
        }
    }

    private Node deleteMin(Node x) {
        if (x.left == null) {
            return x.right;
        }
        x.left = deleteMin(x.left);
        x.N = size(x.left) + size(x.right) + 1;
        return x;
    }

    private class ExpiredKeyReaper extends ShutdownableThread {
        public ExpiredKeyReaper(String name, boolean isInterruptible) {
            super("ExpiredKeyReaper" + name, isInterruptible);
        }

        @Override
        public void doWork() throws InterruptedException {
            timeoutTimer.advanceClock(200L);
        }
    }
}
