package com.videlalvaro;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Phaser;
import java.util.concurrent.atomic.AtomicBoolean;

abstract public class ShutdownableThread extends Thread {

    public boolean isInterruptible = false;
    public AtomicBoolean isRunning = new AtomicBoolean(true);
    private CountDownLatch shutdownLatch = new CountDownLatch(1);

    public ShutdownableThread(String name, boolean isInterruptible) {
        super(name);
        this.setDaemon(false);
    }

    public void shutdown() {
        initiateShutdown();
        try {
            awaitShutdown();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    public boolean initiateShutdown() {
        if (isRunning.compareAndSet(true, false)) {
            isRunning.set(false);
            if (isInterruptible) {
                interrupt();
            }
            return true;
        } else {
            return false;
        }
    }

    public void awaitShutdown() throws InterruptedException {
        shutdownLatch.await();
    }

    abstract public void doWork() throws InterruptedException;

    @Override
    public void run() {
        try {
            while (isRunning.get()) {
                doWork();
            }
        } catch (Exception e) {
            if (isRunning.get()) {
                System.out.println(e.getMessage());
            }
        }
        shutdownLatch.countDown();
        System.out.println("Stopped");
    }
}
