package com.videlalvaro;

import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Timer {
    private ExecutorService taskExecutor;
    private DelayQueue<TimerTaskList> delayQueue = new DelayQueue<>();
    private AtomicInteger taskCounter =  new AtomicInteger(0);
    private TimingWheel timingWheel;

    private ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private ReentrantReadWriteLock.ReadLock readLock = readWriteLock.readLock();
    private ReentrantReadWriteLock.WriteLock writeLock = readWriteLock.writeLock();

    public Timer(ExecutorService taskExecutor, long tickMs, int wheelSize, long startMs) {
        this.taskExecutor = taskExecutor;
        this.timingWheel = new TimingWheel(tickMs, wheelSize, startMs, taskCounter, delayQueue);
    }

    public void add(TimerTask timerTask) {
        readLock.lock();
        try {
            addTimerTaskEntry(new TimerTaskEntry(timerTask));
        } finally {
            readLock.unlock();
        }
    }

    private void addTimerTaskEntry(TimerTaskEntry timerTaskEntry) {
        if (!timingWheel.add(timerTaskEntry)) {
            if (!timerTaskEntry.cancelled()) {
                taskExecutor.submit(timerTaskEntry.timerTask);
            }
        }
    }

    public boolean advanceClock(long timeoutMs) throws InterruptedException {
        TimerTaskList bucket = delayQueue.poll(timeoutMs, TimeUnit.MILLISECONDS);
        if (bucket != null) {
            writeLock.lock();
            try {
                while (bucket != null) {
                    timingWheel.advanceClock(bucket.getExpiration());
                    bucket.flush(this);
                    bucket = delayQueue.poll();
                }
            } finally {
                writeLock.unlock();
            }
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        return taskCounter.get();
    }

    private class TimingWheel {
        private long tickMs;
        private int wheelSize;
        private AtomicInteger taskCounter;
        private DelayQueue<TimerTaskList> queue;

        private long interval;
        private TimerTaskList[] buckets;
        private long currentTime;

        volatile private TimingWheel overflowWheel = null;

        public TimingWheel(long tickMs, int wheelSize, long startMs, AtomicInteger taskCounter,
                     DelayQueue<TimerTaskList> queue) {
            this.tickMs = tickMs;
            this.wheelSize = wheelSize;
            this.taskCounter = taskCounter;
            this.queue = queue;

            this.interval = tickMs * wheelSize;
            this.buckets = new TimerTaskList[wheelSize];
            for (int i = 0; i < wheelSize; i++) {
                this.buckets[i] = new TimerTaskList(taskCounter);
            }
            this.currentTime = startMs - (startMs % tickMs);
        }

        synchronized private void addOverflowWheel() {
            if (overflowWheel == null) {
                this.overflowWheel = new TimingWheel(
                        interval,
                        wheelSize,
                        currentTime,
                        taskCounter,
                        queue
                );
            }
        }

        public boolean add(TimerTaskEntry timerTaskEntry) {
            long expiration = timerTaskEntry.timerTask.getExpirationMs();

            if (timerTaskEntry.cancelled()) {
                return false;
            } else if (expiration < currentTime + tickMs) {
                return false;
            } else if (expiration < currentTime + interval) {
                long virtualId = expiration / tickMs;
                TimerTaskList bucket = buckets[(int)(virtualId % wheelSize)];
                bucket.add(timerTaskEntry);

                if (bucket.setExpiration(virtualId * tickMs)) {
                    queue.offer(bucket);
                }
                return true;
            } else {
                if (overflowWheel == null) {
                    addOverflowWheel();
                }
                return overflowWheel.add(timerTaskEntry);
            }
        }

        public void advanceClock(long timeMs) {
            if (timeMs >= currentTime + tickMs) {
                currentTime = timeMs - (timeMs % tickMs);

                if (overflowWheel != null) {
                    overflowWheel.advanceClock(currentTime);
                }
            }
        }
    }

    private class TimerTaskList implements Delayed {

        AtomicInteger taskCounter;

        private TimerTaskEntry root;

        private AtomicLong expiration = new AtomicLong(-1L);

        public TimerTaskList(AtomicInteger taskCounter) {
            this.taskCounter = taskCounter;
            root = new TimerTaskEntry(null);
            root.next = root;
            root.prev = root;
        }

        public boolean setExpiration(long expirationMs) {
            return expiration.getAndSet(expirationMs) != expirationMs;
        }

        public long getExpiration() {
            return expiration.get();
        }

        public void add(TimerTaskEntry timerTaskEntry) {
            boolean done = false;
            while (!done) {
                timerTaskEntry.remove();

                synchronized (this) {
                    synchronized (timerTaskEntry) {
                        if (timerTaskEntry.list == null) {
                            TimerTaskEntry tail = root.prev;
                            timerTaskEntry.next = root;
                            timerTaskEntry.prev = tail;
                            timerTaskEntry.list = this;
                            tail.next = timerTaskEntry;
                            root.prev = timerTaskEntry;
                            taskCounter.incrementAndGet();
                            done = true;
                        }
                    }
                }
            }
        }

        public synchronized void remove(TimerTaskEntry timerTaskEntry) {
            synchronized (timerTaskEntry) {
                if (timerTaskEntry.list.equals(this)) {
                    timerTaskEntry.next.prev = timerTaskEntry.prev;
                    timerTaskEntry.prev.next = timerTaskEntry.next;
                    timerTaskEntry.next = null;
                    timerTaskEntry.prev = null;
                    timerTaskEntry.list = null;
                    taskCounter.decrementAndGet();
                }
            }
        }

        public synchronized void flush(Timer timer) {
            TimerTaskEntry head = root.next;
            while (!head.equals(root)) {
                remove(head);
                timer.addTimerTaskEntry(head);
                head = root.next;
            }
            expiration.set(-1L);
        }

        public long getDelay(TimeUnit unit) {
            return unit.convert(Math.max(getExpiration() - System.currentTimeMillis(), 0), TimeUnit.MILLISECONDS);
        }

        public int compareTo(Delayed d) {
            TimerTaskList other = (TimerTaskList) d;

            if (getExpiration() < other.getExpiration()) {
                return -1;
            }
            else if (getExpiration() > other.getExpiration()) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }

    class TimerTaskEntry {

        public volatile TimerTaskList list = null;
        public volatile TimerTaskEntry next = null;
        public volatile TimerTaskEntry prev = null;

        public TimerTask timerTask = null;

        public TimerTaskEntry(TimerTask timerTask) {
            if (timerTask != null) {
                timerTask.setTimerTaskEntry(this);
            }
            this.timerTask = timerTask;
        }

        public boolean cancelled() {
            return timerTask.getTimerTaskEntry() != this;
        }

        public void remove() {
            TimerTaskList currentList = list;
            while (currentList != null) {
                currentList.remove(this);
                currentList = list;
            }
        }
    }
}
