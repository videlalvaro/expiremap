package com.videlalvaro;

import com.videlalvaro.Timer.TimerTaskEntry;

abstract class TimerTask implements Runnable {
    protected long expirationMs;

    private TimerTaskEntry timerTaskEntry = null;

    public synchronized void cancel() {
        if (timerTaskEntry != null) {
            timerTaskEntry.remove();
        }
    }

    synchronized void setTimerTaskEntry(TimerTaskEntry entry) {
        if (timerTaskEntry != null && timerTaskEntry != entry) {
            timerTaskEntry.remove();
        }
        this.timerTaskEntry = entry;
    }

    TimerTaskEntry getTimerTaskEntry() {
        return timerTaskEntry;
    }

    public long getExpirationMs() {
        return this.expirationMs;
    }

    public void setExpirationMs(long expirationMs) {
        this.expirationMs = expirationMs;
    }
}
