package com.videlalvaro;

import org.junit.After;
import org.junit.Before;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.*;

public class TimerTest {

    private class TestTask extends TimerTask {
        private int id;
        private CountDownLatch latch;
        private ArrayList<Integer> output;

        private AtomicBoolean completed = new AtomicBoolean(false);

        public TestTask(long expirationMs, int id, CountDownLatch latch, ArrayList<Integer> output) {
            this.setExpirationMs(expirationMs);
            this.id = id;
            this.latch = latch;
            this.output = output;
        }

        @Override
        public void run() {
            if (completed.compareAndSet(false, true)) {
                synchronized (this) {
                    output.add(new Integer(id));
                    latch.countDown();
                }
            }
        }
    }

    private ExecutorService executor = null;

    @Before
    public void setUp() throws Exception {
        this.executor = Executors.newSingleThreadExecutor();
    }

    @After
    public void tearDown() throws Exception {
        executor.shutdown();
        executor = null;
    }

    @org.junit.Test
    public void testAlreadyExpiredTask() throws Exception {
        long startTime = System.currentTimeMillis();
        Timer timer = new Timer(executor, 1, 3, startTime);
        ArrayList<Integer> output = new ArrayList<>(5);
        ArrayList<CountDownLatch> latches = new ArrayList<>(5);

        for (int i = -5; i < 0; i++) {
            CountDownLatch latch = new CountDownLatch(1);
            timer.add(new TestTask(startTime + i, i, latch, output));
            latches.add(latch);
        }

        for (CountDownLatch l: latches) {
            assertEquals(true, l.await(3, TimeUnit.SECONDS));
        }

        HashSet<Integer> expected = new HashSet<>(Arrays.asList(new Integer[] {-5, -4, -3, -2, -1}));
        HashSet<Integer> actual = new HashSet<>(output);
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testTaskExpiration() throws Exception {
        long startTime  = System.currentTimeMillis();
        Timer timer = new Timer(executor, 1, 3, startTime);
        ArrayList<Integer> output = new ArrayList<>(500);
        ArrayList<CountDownLatch> latches = new ArrayList<>(500);

        ArrayList<TestTask> tasks = new ArrayList<>(500);
        ArrayList<Integer> ids = new ArrayList<>(500);

        for (int i = 0; i < 5; i++) {
            CountDownLatch latch = new CountDownLatch(1);
            tasks.add(new TestTask(startTime + i, i, latch, output));
            ids.add(new Integer(i));
            latches.add(latch);
        }

        for (int i = 10; i < 100; i++) {
            CountDownLatch latch = new CountDownLatch(2);
            tasks.add(new TestTask(startTime + i, i, latch, output));
            tasks.add(new TestTask(startTime + i, i, latch, output));
            ids.add(new Integer(i));
            ids.add(new Integer(i));
            latches.add(latch);
        }

        for (int i = 100; i < 500; i++) {
            CountDownLatch latch = new CountDownLatch(1);
            tasks.add(new TestTask(startTime + i, i, latch, output));
            ids.add(new Integer(i));
            latches.add(latch);
        }

        Collections.shuffle(tasks);

        for (TestTask t: tasks) {
            timer.add(t);
        }

        while (timer.advanceClock(1000)) {}

        for (CountDownLatch l: latches) {
            l.await();
        }

        Collections.sort(ids);
        assertEquals(ids, output);
    }
}