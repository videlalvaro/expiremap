package com.videlalvaro;

import static org.junit.Assert.assertEquals;

public class ExpireMapTest {

    @org.junit.Test
    public void testExpireMap() throws Exception {
        ExpireMap<String, String> expireMap = new ExpireMap<>();
        expireMap.put("a", "A", 3500L);
        expireMap.put("b", "B", 500L);
        expireMap.put("c", "C", -1L);
        expireMap.put("d", "D", 1500L);
        assertEquals("A", expireMap.get("a"));
        assertEquals("B", expireMap.get("b"));
        assertEquals("C", expireMap.get("c"));
        assertEquals("D", expireMap.get("d"));
        Thread.sleep(1000);
        assertEquals("A", expireMap.get("a"));
        assertEquals(null, expireMap.get("b"));
        assertEquals(null, expireMap.get("c"));
        assertEquals("D", expireMap.get("d"));
        expireMap.remove("d");
        assertEquals("A", expireMap.get("a"));
        assertEquals(null, expireMap.get("d"));
        Thread.sleep(1000);
        assertEquals("A", expireMap.get("a"));
        expireMap.put("d", "D", 2500);
        Thread.sleep(2000);
        assertEquals(null, expireMap.get("a"));
        assertEquals("D", expireMap.get("d"));
        expireMap.remove("d");
        assertEquals(null, expireMap.get("d"));
    }
}